// the MIDI channel number to send messages
const int channel = 1;
 
//Definir les sorties en fonction des canaux midi
const int cc_A=8; const int cc_Abis=18;  // A = pour l'entrée physique A0
const int cc_B=7; const int cc_Bbis=17;  // B = pour l'entrée physique A1 ...etc
const int cc_C=6; const int cc_Cbis=16;
const int cc_D=5; const int cc_Dbis=15;
const int cc_E=4; const int cc_Ebis=14;
const int cc_F=3; const int cc_Fbis=13;
const int cc_G=2; const int cc_Gbis=12;
const int cc_H=1; const int cc_Hbis=11;
 
 
void setup() {}
 
int previous_n0a = -1; int previous_n0b = -1;
int previous_n1a = -1; int previous_n1b = -1;
int previous_n2a = -1; int previous_n2b = -1;
int previous_n3a = -1; int previous_n3b = -1;
int previous_n4a = -1; int previous_n4b = -1;
int previous_n5a = -1; int previous_n5b = -1;
int previous_n6a = -1; int previous_n6b = -1;
int previous_n7a = -1; int previous_n7b = -1;
 
elapsedMillis msec = 0;
 
void loop() {
  // only check the analog cc_s 50 times per second (20ms),
  // to prevent a flood of MIDI messages
  if (msec >= 20) {
    msec = 0;
    int n0a = analogRead(A0) / 100;
    int n0b = analogRead(A0) - n0a*100;
 
    int n1a = analogRead(A1) / 100;
    int n1b = analogRead(A1) - n1a*100;
 
    int n2a = analogRead(A2) / 100;
    int n2b = analogRead(A2) - n2a*100;
 
    int n3a = analogRead(A3) / 100;
    int n3b = analogRead(A3) - n3a*100;
 
    int n4a = analogRead(A4) / 100;
    int n4b = analogRead(A4) - n4a*100;
 
    int n5a = analogRead(A5) / 100;
    int n5b = analogRead(A5) - n5a*100;
 
    int n6a = analogRead(A6) / 100;
    int n6b = analogRead(A6) - n6a*100;
 
    int n7a = analogRead(A7) / 100;
    int n7b = analogRead(A7) - n7a*100;
 
    // only transmit MIDI messages if analog cc_ changed
    //       usbMIDI.sendControlChange(cc, value, channel);
 
    // control change
    if (n0a != previous_n0a) {
      usbMIDI.sendControlChange(cc_A, n0a, channel);
      previous_n0a = n0a;
    }
 
    if (n0b != previous_n0b) {
      usbMIDI.sendControlChange(cc_Abis, n0b, channel);
      previous_n0b = n0b;
    }
 
    // control change
    if (n1a != previous_n1a) {
      usbMIDI.sendControlChange(cc_B, n1a, channel);
      previous_n1a = n1a;
    }
 
    if (n1b != previous_n1b) {
      usbMIDI.sendControlChange(cc_Bbis, n1b, channel);
      previous_n1b = n1b;
    }    
 
    // control change
    if (n2a != previous_n2a) {
      usbMIDI.sendControlChange(cc_C, n2a, channel);
      previous_n2a = n2a;
    }
 
    if (n2b != previous_n2b) {
      usbMIDI.sendControlChange(cc_Cbis, n2b, channel);
      previous_n2b = n2b;
    }    
 
    // control change
    if (n3a != previous_n3a) {
      usbMIDI.sendControlChange(cc_D, n3a, channel);
      previous_n3a = n3a;
    }
 
    if (n3b != previous_n3b) {
      usbMIDI.sendControlChange(cc_Dbis, n3b, channel);
      previous_n3b = n3b;
    }    
 
    // control change
    if (n4a != previous_n4a) {
      usbMIDI.sendControlChange(cc_E, n4a, channel);
      previous_n4a = n4a;
    }
 
    if (n4b != previous_n4b) {
      usbMIDI.sendControlChange(cc_Ebis, n4b, channel);
      previous_n4b = n4b;
    }   
 
    // control change
    if (n5a != previous_n5a) {
      usbMIDI.sendControlChange(cc_F, n5a, channel);
      previous_n5a = n5a;
    }
 
    if (n5b != previous_n5b) {
      usbMIDI.sendControlChange(cc_Fbis, n5b, channel);
      previous_n5b = n5b;
    }   
 
    // control change
    if (n6a != previous_n6a) {
      usbMIDI.sendControlChange(cc_G, n6a, channel);
      previous_n6a = n6a;
    }
 
    if (n6b != previous_n6b) {
      usbMIDI.sendControlChange(cc_Gbis, n6b, channel);
      previous_n6b = n6b;
    }   
 
    // control change
    if (n7a != previous_n7a) {
      usbMIDI.sendControlChange(cc_H, n7a, channel);
      previous_n7a = n7a;
    }
 
    if (n7b != previous_n7b) {
      usbMIDI.sendControlChange(cc_Hbis, n7b, channel);
      previous_n7b = n7b;
    }   
 
  }
 
  // MIDI Controllers should discard incoming MIDI messages.
  // http://forum.pjrc.com/threads/24179-Teensy-3-Ableton-Analog-CC-causes-midi-crash
  while (usbMIDI.read()) {
    // ignore incoming messages
  }
}
