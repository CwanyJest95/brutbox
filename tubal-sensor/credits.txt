http://fablab.cc-parthenay.fr/?projects=tubal-musique

Le TubaL a été imaginé par des élèves de 2nd du lycée E. Pérochon (Parthenay – 79) dans le cadre de l’enseignement d’exploration Création et Innovation Technologique pendant l’année scolaire 2016-2017, suite à la proposition du Collectif Brut Pop (http://brutpop.blogspot.fr/) de construire des instruments de musique à partir de la Malinette pour les résidents du foyer Les Genêts (http://www.melioris-lesgenetschatillon.fr/). Ce projet a été réalisé grâce au support technique du Parth’Lab.

david.jadaud_at_ac-poitiers.fr