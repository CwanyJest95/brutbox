EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:teensy_V1-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Motherboard BrutBox Teensy 2.0 "
Date "2015-10-07"
Rev "1.0"
Comp "Autor: Jonathan IAPICCO for RESONNANCE NUMERIQUE"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X12 P1
U 1 1 5614E3ED
P 5300 1650
F 0 "P1" H 5300 2300 50  0000 C CNN
F 1 "Left_conn" V 5400 1650 50  0000 C CNN
F 2 "BrutBox_MB_Teensy:Socket_Strip_Straight_1x12" H 5300 1650 60  0001 C CNN
F 3 "" H 5300 1650 60  0000 C CNN
	1    5300 1650
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X12 P2
U 1 1 5614E5EC
P 6500 1650
F 0 "P2" H 6500 2300 50  0000 C CNN
F 1 "Right_conn" V 6600 1650 50  0000 C CNN
F 2 "BrutBox_MB_Teensy:Socket_Strip_Straight_1x12" H 6500 1650 60  0001 C CNN
F 3 "" H 6500 1650 60  0000 C CNN
	1    6500 1650
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X05 P7
U 1 1 5614F4EF
P 3150 4600
F 0 "P7" H 3150 4900 50  0000 C CNN
F 1 "USB1" V 3250 4600 50  0000 C CNN
F 2 "BrutBox_MB_Teensy:USB_A_V_D" H 3150 4600 60  0001 C CNN
F 3 "" H 3150 4600 60  0000 C CNN
	1    3150 4600
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 5614FDD5
P 4950 2350
F 0 "#PWR01" H 4950 2100 50  0001 C CNN
F 1 "GND" H 4950 2200 50  0000 C CNN
F 2 "" H 4950 2350 60  0000 C CNN
F 3 "" H 4950 2350 60  0000 C CNN
	1    4950 2350
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR02
U 1 1 5614FDFD
P 6850 1100
F 0 "#PWR02" H 6850 950 50  0001 C CNN
F 1 "+5V" H 6850 1240 50  0000 C CNN
F 2 "" H 6850 1100 60  0000 C CNN
F 3 "" H 6850 1100 60  0000 C CNN
	1    6850 1100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 56150013
P 3350 4800
F 0 "#PWR03" H 3350 4550 50  0001 C CNN
F 1 "GND" H 3350 4650 50  0000 C CNN
F 2 "" H 3350 4800 60  0000 C CNN
F 3 "" H 3350 4800 60  0000 C CNN
	1    3350 4800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR04
U 1 1 5615035F
P 3350 4400
F 0 "#PWR04" H 3350 4250 50  0001 C CNN
F 1 "+5V" H 3350 4540 50  0000 C CNN
F 2 "" H 3350 4400 60  0000 C CNN
F 3 "" H 3350 4400 60  0000 C CNN
	1    3350 4400
	1    0    0    -1  
$EndComp
Text Label 4600 1200 0    60   ~ 0
0_SS
Text Label 4600 1300 0    60   ~ 0
1_SCLK
Text Label 4600 1400 0    60   ~ 0
2_MOSI
Text Label 4600 1500 0    60   ~ 0
3_MISO
Text Label 4600 1600 0    60   ~ 0
4*
Text Label 4600 1700 0    60   ~ 0
5*_SCL
Text Label 4600 1800 0    60   ~ 0
6_SDA
Text Label 4600 1900 0    60   ~ 0
7_RX
Text Label 4600 2000 0    60   ~ 0
8_TX
Text Label 4600 2100 0    60   ~ 0
9*
Text Label 4600 2200 0    60   ~ 0
10*
Text Label 7050 1400 0    60   ~ 0
A2
Text Label 7050 1500 0    60   ~ 0
A3
Text Label 7050 1700 0    60   ~ 0
A5
Text Label 7050 1800 0    60   ~ 0
A6*
Text Label 7050 1900 0    60   ~ 0
A7*
Text Label 7050 2000 0    60   ~ 0
A8
Text Label 7050 2100 0    60   ~ 0
A9*
Text Label 7050 2200 0    60   ~ 0
A10
Text Notes 650  950  0    60   ~ 0
Teensy 2.0 shematic.\nDon't modify this section or asume your changement.\nJust add "none connection" symbol on pin you don't use.
Text Notes 5650 1700 0    60   ~ 0
Teensy 2.0\n
Text Notes 5600 3350 0    60   ~ 0
USB Connectors\n
$Comp
L PWR_FLAG #FLG07
U 1 1 5615A341
P 9400 1050
F 0 "#FLG07" H 9400 1145 50  0001 C CNN
F 1 "PWR_FLAG" H 9400 1230 50  0000 C CNN
F 2 "" H 9400 1050 60  0000 C CNN
F 3 "" H 9400 1050 60  0000 C CNN
	1    9400 1050
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG08
U 1 1 5615A466
P 9750 1050
F 0 "#FLG08" H 9750 1145 50  0001 C CNN
F 1 "PWR_FLAG" H 9750 1230 50  0000 C CNN
F 2 "" H 9750 1050 60  0000 C CNN
F 3 "" H 9750 1050 60  0000 C CNN
	1    9750 1050
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR09
U 1 1 5615A53B
P 9400 1050
F 0 "#PWR09" H 9400 900 50  0001 C CNN
F 1 "+5V" H 9400 1190 50  0000 C CNN
F 2 "" H 9400 1050 60  0000 C CNN
F 3 "" H 9400 1050 60  0000 C CNN
	1    9400 1050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5615A68B
P 9750 1050
F 0 "#PWR010" H 9750 800 50  0001 C CNN
F 1 "GND" H 9750 900 50  0000 C CNN
F 2 "" H 9750 1050 60  0000 C CNN
F 3 "" H 9750 1050 60  0000 C CNN
	1    9750 1050
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 P15
U 1 1 5615A88D
P 10300 950
F 0 "P15" V 10400 950 50  0000 C CNN
F 1 "H1" V 10400 950 50  0001 C CNN
F 2 "BrutBox_MB_Teensy:1pin" H 10300 950 60  0001 C CNN
F 3 "" H 10300 950 60  0000 C CNN
	1    10300 950 
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P16
U 1 1 5615A970
P 10450 950
F 0 "P16" V 10550 950 50  0000 C CNN
F 1 "H2" V 10550 950 50  0001 C CNN
F 2 "BrutBox_MB_Teensy:1pin" H 10450 950 60  0001 C CNN
F 3 "" H 10450 950 60  0000 C CNN
	1    10450 950 
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P17
U 1 1 5615A9B5
P 10600 950
F 0 "P17" V 10700 950 50  0000 C CNN
F 1 "H3" V 10700 950 50  0001 C CNN
F 2 "BrutBox_MB_Teensy:1pin" H 10600 950 60  0001 C CNN
F 3 "" H 10600 950 60  0000 C CNN
	1    10600 950 
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P18
U 1 1 5615A9FB
P 10750 950
F 0 "P18" V 10850 950 50  0000 C CNN
F 1 "H4" V 10850 950 50  0001 C CNN
F 2 "BrutBox_MB_Teensy:1pin" H 10750 950 60  0001 C CNN
F 3 "" H 10750 950 60  0000 C CNN
	1    10750 950 
	0    -1   -1   0   
$EndComp
NoConn ~ 10300 1150
NoConn ~ 10450 1150
NoConn ~ 10600 1150
NoConn ~ 10750 1150
Text Notes 9750 650  0    60   ~ 0
Power Flag and Holes\n
Text Label 7050 1600 0    60   ~ 0
A4
Text Notes 650  3500 0    60   ~ 0
USB connectors section. \nDraw your shematic here and copi your "Net label".
NoConn ~ 3350 4500
Text Label 7050 1300 0    60   ~ 0
A1
Text Label 7050 1200 0    60   ~ 0
A0
Wire Wire Line
	6850 1100 6700 1100
Wire Wire Line
	5100 1100 4950 1100
Wire Wire Line
	4950 1100 4950 2350
Wire Wire Line
	5100 1200 4600 1200
Wire Wire Line
	5100 1300 4600 1300
Wire Wire Line
	5100 1400 4600 1400
Wire Wire Line
	5100 1500 4600 1500
Wire Wire Line
	5100 1600 4600 1600
Wire Wire Line
	5100 1700 4600 1700
Wire Wire Line
	5100 1800 4600 1800
Wire Wire Line
	5100 1900 4600 1900
Wire Wire Line
	5100 2000 4600 2000
Wire Wire Line
	5100 2100 4600 2100
Wire Wire Line
	5100 2200 4600 2200
Wire Notes Line
	500  3150 11200 3150
Wire Notes Line
	5550 3150 5550 3450
Wire Notes Line
	5550 3450 6350 3450
Wire Notes Line
	6350 3450 6350 3150
Wire Notes Line
	5100 900  6700 900 
Wire Notes Line
	5100 900  5100 2400
Wire Notes Line
	5100 2400 6700 2400
Wire Notes Line
	6700 2400 6700 900 
Wire Notes Line
	9150 500  9150 1600
Wire Notes Line
	9150 1600 11200 1600
Wire Notes Line
	9150 700  11200 700 
Wire Notes Line
	500  1100 3500 1100
Wire Notes Line
	3500 1100 3500 500 
Wire Notes Line
	3200 3650 500  3650
Wire Notes Line
	3200 3150 3200 3650
Wire Wire Line
	3350 4700 3350 4800
$Comp
L CONN_01X05 P8
U 1 1 561FE1EA
P 4150 4600
F 0 "P8" H 4150 4900 50  0000 C CNN
F 1 "USB2" V 4250 4600 50  0000 C CNN
F 2 "BrutBox_MB_Teensy:USB_A_V_D" H 4150 4600 60  0001 C CNN
F 3 "" H 4150 4600 60  0000 C CNN
	1    4150 4600
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 561FE1F0
P 4350 4800
F 0 "#PWR013" H 4350 4550 50  0001 C CNN
F 1 "GND" H 4350 4650 50  0000 C CNN
F 2 "" H 4350 4800 60  0000 C CNN
F 3 "" H 4350 4800 60  0000 C CNN
	1    4350 4800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR014
U 1 1 561FE1F6
P 4350 4400
F 0 "#PWR014" H 4350 4250 50  0001 C CNN
F 1 "+5V" H 4350 4540 50  0000 C CNN
F 2 "" H 4350 4400 60  0000 C CNN
F 3 "" H 4350 4400 60  0000 C CNN
	1    4350 4400
	1    0    0    -1  
$EndComp
NoConn ~ 4350 4500
Wire Wire Line
	4350 4700 4350 4800
$Comp
L CONN_01X05 P9
U 1 1 561FE3DB
P 5150 4600
F 0 "P9" H 5150 4900 50  0000 C CNN
F 1 "USB3" V 5250 4600 50  0000 C CNN
F 2 "BrutBox_MB_Teensy:USB_A_V_D" H 5150 4600 60  0001 C CNN
F 3 "" H 5150 4600 60  0000 C CNN
	1    5150 4600
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 561FE3E1
P 5350 4800
F 0 "#PWR015" H 5350 4550 50  0001 C CNN
F 1 "GND" H 5350 4650 50  0000 C CNN
F 2 "" H 5350 4800 60  0000 C CNN
F 3 "" H 5350 4800 60  0000 C CNN
	1    5350 4800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR016
U 1 1 561FE3E7
P 5350 4400
F 0 "#PWR016" H 5350 4250 50  0001 C CNN
F 1 "+5V" H 5350 4540 50  0000 C CNN
F 2 "" H 5350 4400 60  0000 C CNN
F 3 "" H 5350 4400 60  0000 C CNN
	1    5350 4400
	1    0    0    -1  
$EndComp
NoConn ~ 5350 4500
Wire Wire Line
	5350 4700 5350 4800
$Comp
L CONN_01X05 P10
U 1 1 561FE3F0
P 6150 4600
F 0 "P10" H 6150 4900 50  0000 C CNN
F 1 "USB4" V 6250 4600 50  0000 C CNN
F 2 "BrutBox_MB_Teensy:USB_A_V_D" H 6150 4600 60  0001 C CNN
F 3 "" H 6150 4600 60  0000 C CNN
	1    6150 4600
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR017
U 1 1 561FE3F6
P 6350 4800
F 0 "#PWR017" H 6350 4550 50  0001 C CNN
F 1 "GND" H 6350 4650 50  0000 C CNN
F 2 "" H 6350 4800 60  0000 C CNN
F 3 "" H 6350 4800 60  0000 C CNN
	1    6350 4800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR018
U 1 1 561FE3FC
P 6350 4400
F 0 "#PWR018" H 6350 4250 50  0001 C CNN
F 1 "+5V" H 6350 4540 50  0000 C CNN
F 2 "" H 6350 4400 60  0000 C CNN
F 3 "" H 6350 4400 60  0000 C CNN
	1    6350 4400
	1    0    0    -1  
$EndComp
NoConn ~ 6350 4500
Wire Wire Line
	6350 4700 6350 4800
$Comp
L CONN_01X05 P11
U 1 1 561FE4FD
P 7150 4600
F 0 "P11" H 7150 4900 50  0000 C CNN
F 1 "USB5" V 7250 4600 50  0000 C CNN
F 2 "BrutBox_MB_Teensy:USB_A_V_D" H 7150 4600 60  0001 C CNN
F 3 "" H 7150 4600 60  0000 C CNN
	1    7150 4600
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 561FE503
P 7350 4800
F 0 "#PWR019" H 7350 4550 50  0001 C CNN
F 1 "GND" H 7350 4650 50  0000 C CNN
F 2 "" H 7350 4800 60  0000 C CNN
F 3 "" H 7350 4800 60  0000 C CNN
	1    7350 4800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR020
U 1 1 561FE509
P 7350 4400
F 0 "#PWR020" H 7350 4250 50  0001 C CNN
F 1 "+5V" H 7350 4540 50  0000 C CNN
F 2 "" H 7350 4400 60  0000 C CNN
F 3 "" H 7350 4400 60  0000 C CNN
	1    7350 4400
	1    0    0    -1  
$EndComp
NoConn ~ 7350 4500
Wire Wire Line
	7350 4700 7350 4800
$Comp
L GND #PWR021
U 1 1 561FE518
P 8350 4800
F 0 "#PWR021" H 8350 4550 50  0001 C CNN
F 1 "GND" H 8350 4650 50  0000 C CNN
F 2 "" H 8350 4800 60  0000 C CNN
F 3 "" H 8350 4800 60  0000 C CNN
	1    8350 4800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR022
U 1 1 561FE51E
P 8350 4400
F 0 "#PWR022" H 8350 4250 50  0001 C CNN
F 1 "+5V" H 8350 4540 50  0000 C CNN
F 2 "" H 8350 4400 60  0000 C CNN
F 3 "" H 8350 4400 60  0000 C CNN
	1    8350 4400
	1    0    0    -1  
$EndComp
NoConn ~ 8350 4500
Wire Wire Line
	8350 4700 8350 4800
$Comp
L CONN_01X05 P13
U 1 1 561FE527
P 9150 4600
F 0 "P13" H 9150 4900 50  0000 C CNN
F 1 "USB7" V 9250 4600 50  0000 C CNN
F 2 "BrutBox_MB_Teensy:USB_A_V_D" H 9150 4600 60  0001 C CNN
F 3 "" H 9150 4600 60  0000 C CNN
	1    9150 4600
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR023
U 1 1 561FE52D
P 9350 4800
F 0 "#PWR023" H 9350 4550 50  0001 C CNN
F 1 "GND" H 9350 4650 50  0000 C CNN
F 2 "" H 9350 4800 60  0000 C CNN
F 3 "" H 9350 4800 60  0000 C CNN
	1    9350 4800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR024
U 1 1 561FE533
P 9350 4400
F 0 "#PWR024" H 9350 4250 50  0001 C CNN
F 1 "+5V" H 9350 4540 50  0000 C CNN
F 2 "" H 9350 4400 60  0000 C CNN
F 3 "" H 9350 4400 60  0000 C CNN
	1    9350 4400
	1    0    0    -1  
$EndComp
NoConn ~ 9350 4500
Wire Wire Line
	9350 4700 9350 4800
$Comp
L CONN_01X05 P14
U 1 1 561FE53C
P 10150 4600
F 0 "P14" H 10150 4900 50  0000 C CNN
F 1 "USB8" V 10250 4600 50  0000 C CNN
F 2 "BrutBox_MB_Teensy:USB_A_V_D" H 10150 4600 60  0001 C CNN
F 3 "" H 10150 4600 60  0000 C CNN
	1    10150 4600
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR025
U 1 1 561FE542
P 10350 4800
F 0 "#PWR025" H 10350 4550 50  0001 C CNN
F 1 "GND" H 10350 4650 50  0000 C CNN
F 2 "" H 10350 4800 60  0000 C CNN
F 3 "" H 10350 4800 60  0000 C CNN
	1    10350 4800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR026
U 1 1 561FE548
P 10350 4400
F 0 "#PWR026" H 10350 4250 50  0001 C CNN
F 1 "+5V" H 10350 4540 50  0000 C CNN
F 2 "" H 10350 4400 60  0000 C CNN
F 3 "" H 10350 4400 60  0000 C CNN
	1    10350 4400
	1    0    0    -1  
$EndComp
NoConn ~ 10350 4500
Wire Wire Line
	10350 4700 10350 4800
$Comp
L CONN_01X01 P19
U 1 1 56DEE79A
P 10900 950
F 0 "P19" V 11000 950 50  0000 C CNN
F 1 "H4" V 11000 950 50  0001 C CNN
F 2 "BrutBox_MB_Teensy:1pin" H 10900 950 60  0001 C CNN
F 3 "" H 10900 950 60  0000 C CNN
	1    10900 950 
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P20
U 1 1 56DEE7EB
P 11050 950
F 0 "P20" V 11150 950 50  0000 C CNN
F 1 "H4" V 11150 950 50  0001 C CNN
F 2 "BrutBox_MB_Teensy:1pin" H 11050 950 60  0001 C CNN
F 3 "" H 11050 950 60  0000 C CNN
	1    11050 950 
	0    -1   -1   0   
$EndComp
NoConn ~ 10900 1150
NoConn ~ 11050 1150
Wire Wire Line
	6700 1200 7050 1200
Wire Wire Line
	7050 1300 6700 1300
Wire Wire Line
	6700 1400 7050 1400
Wire Wire Line
	7050 1500 6700 1500
Wire Wire Line
	6700 1600 7050 1600
Wire Wire Line
	7050 1700 6700 1700
Wire Wire Line
	6700 1800 7050 1800
Wire Wire Line
	7050 1900 6700 1900
Wire Wire Line
	7050 2000 6700 2000
Wire Wire Line
	6700 2100 7050 2100
Wire Wire Line
	7050 2200 6700 2200
Text Label 3350 4600 0    60   ~ 0
A0
Text Label 4350 4600 0    60   ~ 0
A1
Text Label 5350 4600 0    60   ~ 0
A2
Text Label 6350 4600 0    60   ~ 0
A3
Text Label 7350 4600 0    60   ~ 0
A4
Text Label 8350 4600 0    60   ~ 0
A5
Text Label 9350 4600 0    60   ~ 0
A6*
Text Label 10350 4600 0    60   ~ 0
A7*
$Comp
L CONN_01X05 P12
U 1 1 561FE512
P 8150 4600
F 0 "P12" H 8150 4900 50  0000 C CNN
F 1 "USB6" V 8250 4600 50  0000 C CNN
F 2 "BrutBox_MB_Teensy:USB_A_V_D" H 8150 4600 60  0001 C CNN
F 3 "" H 8150 4600 60  0000 C CNN
	1    8150 4600
	-1   0    0    -1  
$EndComp
Text Notes 5450 3950 0    60   ~ 0
Analogique pin sur D+
$EndSCHEMATC
